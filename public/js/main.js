function shuffle(arr) {
  return arr.sort(() => Math.random() - 0.5);
}

function createAnswers(lang, rightIndex, answersNum = 4) {
  let answers = [strings[lang][rightIndex]];

  function addWrongAnswer() {
    let randomIndex = Math.floor(Math.random() * strings[lang].length),
      wrongAnswer = strings[lang][randomIndex];
    return answers.includes(wrongAnswer) ? addWrongAnswer() : wrongAnswer;
  }

  function createHtml(str, valid) {
    return `<button class="button is-rounded is-fullwidth my-2" data-valid="${valid}" onclick="solve(this)">
      ${str}
    </button>`;
  }
  for (let i = 0; i < answersNum; i++) {
    answers.push(addWrongAnswer());
  }
  answers = shuffle(answers.map((e, i) => createHtml(e, i == 0)));
  return `
  <div>
    <h3 class="mt-2">Guest <code>${lang}</code> language:</h3>
    <div class="answers">
      ${answers.join("")}
    </div>
  </div>`;
}

function solve(btn) {
  if (btn.classList.length == 4) {
    let valid = btn.dataset.valid == "true",
      element = btn.parentElement.firstElementChild,
      unwantedEls = [];
    valid ? btn.classList.add("is-success") : btn.classList.add("is-danger");
    while (element) {
      if (element != btn) {
        element.dataset.valid == "true" ? element.classList.add("is-info") : unwantedEls.push(element);
      }
      element = element.nextElementSibling;
    }
    for (unwanted of unwantedEls) {
      unwanted.remove();
    }
  }
}

var questions = [];
for (let i = 0; i < strings.original.length; i++) {
  let audio = `clips/clip_${('00000'+i).slice(-5)}.mp3`,
    question = "";
  for (let key of Object.keys(strings)) {
    let answers = createAnswers(key, i);
    question = key == "original" ? answers + question : question + answers;
  }
  questions.push(`
  <div class="container mb-4 question">
    <h2>Question</h2>
    <audio controls>
      <source src="${audio}" type="audio/mp4">
    </audio>
    ${question}
  </div>`);
}
document.body.insertAdjacentHTML("beforeend", shuffle(questions).join(""));
