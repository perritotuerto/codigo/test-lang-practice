import re
import srt
from pathlib import Path

root = Path(__file__).parent.parent
paths = (root / "assets").glob("*.srt")
text = ""

for path in paths:
    strs = []
    lang = "original" if len(path.suffixes) == 1 else path.suffixes[0][1:]
    subs = srt.parse(path.read_text())
    for sub in subs:
        strs.append(re.sub(r"<[^>]+?>", "", re.sub(r"\s+", " ", sub.content)))
    text += "  %s: %s,\n" % (lang, str(strs))

text = "const strings = {\n%s}\n" % text
Path(root / "public" / "js" / "strings.js").write_text(text)

